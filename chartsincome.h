#ifndef CHARTSINCOME_H
#define CHARTSINCOME_H
#include <QBarSet>
#include <QBarSeries>
#include <QChart>
#include <QChartView>
#include <QBarCategoryAxis>
#include <QString>

// 数据传输定义
struct ChartsIncomeDTO
{
    qreal expend;
    qreal revenue;
    QString month;
};

class ChartsIncome
{
public:
    ChartsIncome();
    ~ChartsIncome();

    QChartView* create(const QVector<ChartsIncomeDTO> &dataList);


private:
    QChartView *chartView;

    // 收入支出数据
    QVector<ChartsIncomeDTO> m_revenueExpendList;
};

#endif // CHARTSINCOME_H
