#include "dialog_add_revenue.h"
#include "ui_Dialog_add_revenue.h"

Dialog_add_revenue::Dialog_add_revenue(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog_add_revenue)
{
    ui->setupUi(this);
}

Dialog_add_revenue::~Dialog_add_revenue()
{
    delete ui;
}

/**
* 获取表单数据
*/
Ui::Dialog_add_revenue_DTO Dialog_add_revenue::getData()
{
    Ui::Dialog_add_revenue_DTO dto;
    dto.phone = ui->lineEdit_phone->text();
    dto.name = ui->lineEdit_name->text();
    dto.isVip = ui->checkBox_isVip->isChecked();
    dto.position = ui->comboBox_position->currentIndex();
    dto.expendMoney = ui->doubleSpinBox_expend->value();
    dto.incomeMoney = ui->doubleSpinBox_income->value();
    return dto;
}

/**
* 设置表单数据
*/
void Dialog_add_revenue::setData(Ui::Dialog_add_revenue_DTO& dto)
{
    this->dto = dto;
    ui->lineEdit_phone->setText(dto.phone);
    ui->lineEdit_name->setText(dto.name);
    ui->checkBox_isVip->setChecked(dto.isVip);
    ui->comboBox_position->setCurrentIndex(dto.position);
    ui->doubleSpinBox_expend->setValue(dto.expendMoney);
    ui->doubleSpinBox_income->setValue(dto.incomeMoney);
}
