#include "dialog_add_position.h"
#include "ui_dialog_add_position.h"
#include <QMessageBox>

Dialog_add_position::Dialog_add_position(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog_add_position)
{
    ui->setupUi(this);
    // 默认不显示删除按钮
    ui->pushButton_deletePosition->hide();
}

Dialog_add_position::~Dialog_add_position()
{
    delete ui;
}

// 设置数据
void Dialog_add_position::setData(Ui::Dialog_add_position_DTO dto)
{
    this->dto = dto;
    ui->lineEdit_position_name->setText(dto.name);
    ui->lineEdit_remark->setText(dto.remark);
    // 根据id判断是否为编辑模式
    if (dto.id > 0)
    {
        // 显示删除按钮
        ui->pushButton_deletePosition->show();
    }
    else
    {
        // 隐藏删除按钮
        ui->pushButton_deletePosition->hide();
    }
}

// 获取数据
Ui::Dialog_add_position_DTO Dialog_add_position::getData()
{
    return this->dto;
}

// 重载accept
void Dialog_add_position::accept()
{
    emit do_getData(this->getData());
    QDialog::accept();
}

// 点击确定
void Dialog_add_position::on_buttonBox_accepted()
{
}

// 点击取消
void Dialog_add_position::on_buttonBox_rejected()
{
}

// 当输入的区域名称改变时
void Dialog_add_position::on_lineEdit_position_name_textChanged(const QString &arg1)
{
    this->dto.name = arg1;
}

// 当输入的备注改变时
void Dialog_add_position::on_lineEdit_remark_textChanged(const QString &arg1)
{
    this->dto.remark = arg1;
}

// 弹窗删除按钮点击时
void Dialog_add_position::on_pushButton_deletePosition_clicked()
{
    // 显示确认对话框
    QMessageBox::StandardButton reply= QMessageBox::question(this, "警告", "确认要删除\"" + this->dto.name + "\"吗?",  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        emit do_deleteData(this->dto);
    } else {

    }
}

