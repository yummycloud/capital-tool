#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "dialog_add_expend.h"
#include "dialog_add_revenue.h"
#include "dialog_add_position.h"
#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QStringList>
#include "datastore.h"
#include "chartsincome.h"
#include <QTableWidgetItem>
#include <QSqlQuery>
#include <QTableView>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QList>

QT_BEGIN_NAMESPACE

namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    // db新增区域
    void dbAddPosition(Ui::Dialog_add_position_DTO dto);
    // db查询区域
    QList<Ui::Dialog_add_position_DTO> dbQueryPosition(Ui::Dialog_add_position_DTO dto = Ui::Dialog_add_position_DTO{.id=0, .name="", .remark=""});
    // db删除区域
    void dbDeletePosition(Ui::Dialog_add_position_DTO dto);

private slots:
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_actNewExpend_triggered();

    void on_actNewRevenue_triggered();

    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

    void on_pushButton_addPosition_clicked();

    void on_actNewPosition_triggered();

    void on_tableView_position_doubleClicked(const QModelIndex &index);

    void on_pushButton_positionSearch_clicked();

private:
    Ui::MainWindow *ui;

    // 弹窗 ui
    Dialog_add_expend *dialogNewExpend;
    Dialog_add_revenue *dialogNewRevenue;
    Dialog_add_position *dialogAddPosition;

    // db
    Datastore *m_datastore;

    // charts widget
    ChartsIncome *m_chartsincome;

    // 区域管理 tableview 用
    QStandardItemModel *m_tableviewPositionModel;

};
#endif // MAINWINDOW_H
