#include "chartsincome.h"

ChartsIncome::ChartsIncome() {

}
ChartsIncome::~ChartsIncome() {
    delete chartView;
}

QChartView* ChartsIncome::create(const QVector<ChartsIncomeDTO> &dataList)
{
    QBarSet *set0 = new QBarSet("支出");
    QBarSet *set1 = new QBarSet("收入");

    QStringList categories;

    for(const ChartsIncomeDTO &ite : dataList)
    {
        *set0 << ite.expend;
        *set1 << ite.revenue;
        categories << ite.month;
    }

    QBarSeries *series = new QBarSeries();
    series->append(set0);
    series->append(set1);

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(categories);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("近6个月收支可视化");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    chart->createDefaultAxes();
    chart->addAxis(axis, Qt::AlignBottom);
    series->attachAxis(axis);

    chartView = new QChartView(chart);

    return chartView;
}
