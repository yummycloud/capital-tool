#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "datastore.h"
#include "def.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->treeWidget->expandAll();

    // 子窗口
    dialogNewExpend     = new Dialog_add_expend(this);
    dialogNewRevenue    = new Dialog_add_revenue(this);
    dialogAddPosition   = new Dialog_add_position(this);

    // toolbar样式
    ui->toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    // 默认页面
    ui->stackedWidget->setCurrentIndex(0);

    // 设置lcd默认样式
    ui->lcdNumber_totalClient->display("0000000");
    ui->lcdNumber_totalClient->setSegmentStyle(QLCDNumber::Flat);
    ui->lcdNumber_revenue->display("00000000.00");
    ui->lcdNumber_revenue->setSegmentStyle(QLCDNumber::Flat);
    ui->lcdNumber_expend->display("00000000.00");
    ui->lcdNumber_expend->setSegmentStyle(QLCDNumber::Flat);
    ui->lcdNumber_income->display("00000000.00");
    ui->lcdNumber_income->setSegmentStyle(QLCDNumber::Flat);

    // 设置图表柱状图
    ChartsIncome *m_chartsincome = new ChartsIncome();
    // 测试数据
    QVector<ChartsIncomeDTO> dataList = {
        { 1.1,  21.3, "7"},
        { 11.1,  2.3, "8"},
        { 71.1,  9.3, "9"},
    };
    QChartView *chartsViewIncome = m_chartsincome->create(dataList);
    chartsViewIncome->setContentsMargins(0,0,0,0);
    ui->gridLayout_3->addWidget(chartsViewIncome);

    // 数据库操作
    m_datastore = new Datastore;

    // 区域tableView配置
    m_tableviewPositionModel = new QStandardItemModel(this);
    ui->tableView_position->setEditTriggers(QAbstractItemView::NoEditTriggers); // 禁止编辑
    ui->tableView_position->setModel(m_tableviewPositionModel);
    ui->tableView_position->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    QStringList tableView_position_headers{"id", "名称", "备注", "创建时间"};
    QStringList tableView_position_fields{"id", "name", "remark", "create_time"};
    for (int ind = 0; ind < tableView_position_headers.count(); ind++ )
    {
        QStandardItem *headerItem = new QStandardItem(tableView_position_headers[ind]);
        headerItem->setData(tableView_position_fields[ind], Qt::UserRole + ind);
        m_tableviewPositionModel->setHorizontalHeaderItem(ind, headerItem); // 表格标题
    }

    /** ************************************************
     *      信号
     ************************************************ */
    // 区域新增弹窗
    connect(dialogAddPosition, &Dialog_add_position::do_getData, this, &MainWindow::dbAddPosition);
    // 区域新增弹窗中的删除
    connect(dialogAddPosition, &Dialog_add_position::do_deleteData, this, &MainWindow::dbDeletePosition);
}

MainWindow::~MainWindow()
{
    delete m_datastore;
    delete m_chartsincome;
    delete m_tableviewPositionModel;
    delete dialogNewExpend;
    delete dialogAddPosition;
    delete dialogNewRevenue;
    delete ui;
}

// 左侧菜单tree点击
void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    QString itemName = item->text(column);
    // 总览
    if(itemName == Def::TREEITEM_OVERVIEW)
    {
        ui->stackedWidget->setCurrentIndex(Def::TREEITEM_MAP.at(Def::TREEITEM_OVERVIEW));
    }
    // 收支
    else if(itemName == Def::TREEITEM_REVENUE_EXPEND)
    {
        ui->stackedWidget->setCurrentIndex(Def::TREEITEM_MAP.at(Def::TREEITEM_REVENUE_EXPEND));
    }
    // 区域
    else if(itemName == Def::TREEITEM_POSITION_MANAGE)
    {
        ui->stackedWidget->setCurrentIndex(Def::TREEITEM_MAP.at(Def::TREEITEM_POSITION_MANAGE));
        // 查询区域 并设置到tableView
        this->dbQueryPosition();
    }

}

/** **************************************************************************
*       充值消费 revenue & expend
************************************************************************** */
// action新增充值
void MainWindow::on_actNewRevenue_triggered()
{
    dialogNewRevenue->exec(); // 模态
}

// action新增消费
void MainWindow::on_actNewExpend_triggered()
{
    dialogNewExpend->exec(); // 模态
}

// 收入行编辑
void MainWindow::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{
    dialogNewRevenue->exec(); // 模态
}



/** **************************************************************************
*       区域 position
************************************************************************** */
// action新增区域
void MainWindow::on_actNewPosition_triggered()
{
    this->on_pushButton_addPosition_clicked();
}

// 新增区域
void MainWindow::on_pushButton_addPosition_clicked()
{
    dialogAddPosition->exec(); // 模态
    dialogAddPosition->setData(Ui::Dialog_add_position_DTO{
        .id=0,
        .name="",
        .remark="",
    });
}

// db新增区域
void MainWindow::dbAddPosition(Ui::Dialog_add_position_DTO dto)
{
    qDebug() << "dbAddPosition" << dto.name << dto.remark;

    // 判断是否为编辑 id>0则为编辑
    if (dto.id > 0)
    {
        bool ret = m_datastore->positionUpdate(dto);
        if (!ret)
        {
            qDebug() << "执行编辑区域 nok";
        }
        else
        {
            qDebug() << "执行编辑区域 ok";
            // 新增成功则查询tableview
            this->dbQueryPosition();
        }
    }
    else
    {
        bool ret = m_datastore->positionInsert(dto);
        if (!ret)
        {
            qDebug() << "执行新增区域 nok";
        }
        else
        {
            qDebug() << "执行新增区域 ok";
            // 新增成功则查询tableview
            this->dbQueryPosition();
        }
    }

}

// db查询区域
QList<Ui::Dialog_add_position_DTO> MainWindow::dbQueryPosition(Ui::Dialog_add_position_DTO dto)
{
    // 清除第一行之外的行 tableView
    m_tableviewPositionModel->clear();
    if (m_tableviewPositionModel->rowCount() > 1) {
        for (int i = 1; i < m_tableviewPositionModel->rowCount(); ++i) {
            m_tableviewPositionModel->removeRow(i);
        }
    }
    // 获取查询值
    QList<Ui::Dialog_add_position_DTO> retList = m_datastore->positionQuery(dto);
    // 设置到tableView
    int ind = 0;
    for(auto& ite : retList)
    {
        m_tableviewPositionModel->setItem(ind, 0, new QStandardItem(QString::number(ite.id)));
        m_tableviewPositionModel->setItem(ind, 1, new QStandardItem(ite.name));
        m_tableviewPositionModel->setItem(ind, 2, new QStandardItem(ite.remark));
        m_tableviewPositionModel->setItem(ind, 3, new QStandardItem(ite.createTime));
        ind++;
    }
    return retList;
}

// db删除区域
void MainWindow::dbDeletePosition(Ui::Dialog_add_position_DTO dto)
{
    bool ret = m_datastore->positionDelete(dto);
    if (!ret)
    {
        qDebug() << "执行删除区域 nok";
    }
    else
    {
        qDebug() << "执行删除区域 ok";
        // 关闭弹窗
        dialogAddPosition->hide();
        // 删除成功则查询tableview
        this->dbQueryPosition();
    }
}

// 区域 双击行赋值弹窗
void MainWindow::on_tableView_position_doubleClicked(const QModelIndex &index)
{
    // 获取当前单元格所在整行数据
    Ui::Dialog_add_position_DTO dto;
    dto.id      = m_tableviewPositionModel->index(index.row(), 0).data().toInt();
    dto.name    = m_tableviewPositionModel->index(index.row(), 1).data().toString();
    dto.remark  = m_tableviewPositionModel->index(index.row(), 2).data().toString();

    qDebug() << "双击行 =>" << m_tableviewPositionModel->index(index.row(), 1).data();
    dialogAddPosition->setData(dto);
    // 显示弹窗
    on_actNewPosition_triggered();
}

// 区域 点击查询按钮
void MainWindow::on_pushButton_positionSearch_clicked()
{
    QString inputNameStr = ui->lineEdit_positionName->text();
    this->dbQueryPosition(Ui::Dialog_add_position_DTO{ .name = inputNameStr, });
}

