#ifndef DIALOG_ADD_EXPEND_H
#define DIALOG_ADD_EXPEND_H

#include <QDialog>

namespace Ui {
class Dialog_add_expend;
}

class Dialog_add_expend : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_add_expend(QWidget *parent = nullptr);
    ~Dialog_add_expend();

private:
    Ui::Dialog_add_expend *ui;
};

#endif // DIALOG_ADD_EXPEND_H
