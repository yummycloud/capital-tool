#include "datastore.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include "utils.h"

Datastore::Datastore() {
    this->dbConnection();
    sqlQuery = new QSqlQuery;
    this->createDefaultTables();
}

Datastore::~Datastore() {
    delete sqlQuery;
    this->dbClose();
}

void Datastore::dbConnection()
{
    if (QSqlDatabase::contains("qt_sql_default_connection"))
    {
        this->m_database = QSqlDatabase::database("qt_sql_default_connection");
    }
    else
    {
        this->m_database = QSqlDatabase::addDatabase("QSQLITE");
        this->m_database.setDatabaseName(QString(SQLITE_DBNAME) + ".db");
        this->m_database.setUserName(SQLITE_USERNAME);
        this->m_database.setPassword(SQLITE_PASSWORD);
    }

    if (this->m_database.open())
    {
        // QString create_default_sql


    }
}

void Datastore::dbClose()
{
    this->m_database.close();
}

// 获取数据库实例
QSqlQuery* Datastore::getQueryInstance()
{
    return this->sqlQuery;
}


bool Datastore::createDefaultTables()
{
    // 客户表 client
    qDebug() << "创建客户表:";
    sqlQuery->prepare("CREATE TABLE client(id INTEGER PRIMARY KEY AUTOINCREMENT, phone TEXT, name TEXT, wechat_name TEXT, create_time TEXT, is_deleted INTEGER DEFAULT 0);");
    if (!sqlQuery->exec())
    {
        qDebug() << "创建客户表失败";
    }
    // // test
    // // 测试插入
    // sqlQuery->prepare("INSERT INTO client (phone, name)VALUES(:phone,:name);");
    // sqlQuery->bindValue(":phone", "111");
    // sqlQuery->bindValue(":name", "111111");
    // if (!sqlQuery->exec())
    // {
    //     qDebug() << "测试1失败";
    // }
    // // 测试查询
    // sqlQuery->prepare("SELECT * from client;");
    // if (!sqlQuery->exec())
    // {
    //     qDebug() << "测试2失败";
    // }
    // sqlQuery->next();
    // int id = sqlQuery->value("id").toInt();
    // QString phone = sqlQuery->value("phone").toString();
    // QString name = sqlQuery->value("name").toString();
    // QString is_deleted = sqlQuery->value("is_deleted").toString();
    // qDebug() << "id=>" << id << phone << name << is_deleted;

    // 区域表 position
    qDebug() << "创建区域表:";
    sqlQuery->prepare("CREATE TABLE position(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, remark TEXT, create_time TEXT, is_deleted INTEGER DEFAULT 0);");
    if (!sqlQuery->exec())
    {
        qDebug() << "创建区域表失败";
    }


    return true;
}

// 新增区域
bool Datastore::positionInsert(Ui::Dialog_add_position_DTO dto)
{
    sqlQuery->prepare("INSERT INTO position(name, remark, create_time)VALUES(:name, :remark, :create_time);");
    sqlQuery->bindValue(":name", dto.name);
    sqlQuery->bindValue(":remark", dto.remark);
    sqlQuery->bindValue(":create_time", Utils::getCurrentDatetimeStr());
    if (!sqlQuery->exec())
    {
        qDebug() << "新增位置失败";
        return false;
    }
    return true;
}

// 编辑区域
bool Datastore::positionUpdate(Ui::Dialog_add_position_DTO dto)
{
    if (!dto.id)
    {
        return false;
    }
    sqlQuery->prepare("UPDATE position set name=:name, remark=:remark WHERE id=:id;");
    sqlQuery->bindValue(":name", dto.name);
    sqlQuery->bindValue(":remark", dto.remark);
    sqlQuery->bindValue(":id", QString::number(dto.id));
    if (!sqlQuery->exec())
    {
        qDebug() << "编辑位置失败";
        return false;
    }
    return true;
}

// 查询区域
QList<Ui::Dialog_add_position_DTO> Datastore::positionQuery(Ui::Dialog_add_position_DTO dto)
{
    QList<Ui::Dialog_add_position_DTO> retList;
    QString sqlStr = "SELECT * from position where is_deleted = 0 ";
    if (dto.id > 0)             sqlStr += "AND id=:id ";
    if (!dto.name.isEmpty())    sqlStr += "AND name LIKE :name ";
    if (!dto.remark.isEmpty())  sqlStr += "AND remark LIKE :remark ";
    sqlQuery->prepare(sqlStr + ";");
    if (dto.id > 0)             sqlQuery->bindValue(":id", dto.id);
    if (!dto.name.isEmpty())    sqlQuery->bindValue(":name", "%" + dto.name + "%");
    if (!dto.remark.isEmpty())  sqlQuery->bindValue(":remark", "%" + dto.remark + "%");
    if (!sqlQuery->exec())
    {
        qDebug() << "查询区域失败";
        return retList;
    }
    while(sqlQuery->next())
    {
        retList.append(Ui::Dialog_add_position_DTO{
            .id         = sqlQuery->value("id").toLongLong(),
            .name       = sqlQuery->value("name").toString(),
            .remark     = sqlQuery->value("remark").toString(),
            .createTime = sqlQuery->value("create_time").toString(),
        });
    }
    qDebug() << "查询区域 ok" << sqlStr << dto.name;
    return retList;
}

bool Datastore::positionDelete(Ui::Dialog_add_position_DTO dto)
{
    if (!dto.id)
    {
        return false;
    }
    sqlQuery->prepare("UPDATE position set is_deleted=1 WHERE id=:id;");
    sqlQuery->bindValue(":id", QString::number(dto.id));
    if (!sqlQuery->exec())
    {
        qDebug() << "删除位置失败";
        return false;
    }
    return true;
}

