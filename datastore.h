#ifndef DATASTORE_H
#define DATASTORE_H
#include <QSqlDatabase>
#include <QList>

#include "dialog_add_position.h"

#define SQLITE_DBNAME "data"
#define SQLITE_USERNAME "admin"
#define SQLITE_PASSWORD "123456"

class QSqlQuery;

class Datastore
{
public:
    Datastore();
    ~Datastore();

    // 连接数据库
    void dbConnection();
    // 关闭数据库
    void dbClose();
    // 创建默认表
    bool createDefaultTables();
    // 获取数据库实例
    QSqlQuery* getQueryInstance();

    /*
     *  自定义方法
    */
    // 新增区域
    bool positionInsert(Ui::Dialog_add_position_DTO dto);
    // 编辑区域
    bool positionUpdate(Ui::Dialog_add_position_DTO dto);
    // 查询区域
    QList<Ui::Dialog_add_position_DTO> positionQuery(Ui::Dialog_add_position_DTO dto);
    // 删除区域
    bool positionDelete(Ui::Dialog_add_position_DTO dto);

private:
    QSqlDatabase m_database;

    QSqlQuery *sqlQuery;
};

#endif // DATASTORE_H
