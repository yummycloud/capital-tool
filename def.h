#ifndef DEF_H
#define DEF_H
#include <map>

namespace Def
{

// 栈组件索引类型
typedef int StackWidgetIndex_t;
// 菜单名称类型
typedef const char* const TreeItemName_t;
// 树形菜单定义 名称
TreeItemName_t TREEITEM_OVERVIEW               = "总览";
TreeItemName_t TREEITEM_CLIENT                 = "客户";
TreeItemName_t TREEITEM_REVENUE_EXPEND         = "充值消费";
TreeItemName_t TREEITEM_SYSTEM_SETTING         = "系统设置";
TreeItemName_t TREEITEM_POSITION_MANAGE        = "区域管理";
TreeItemName_t TREEITEM_EXPEND_TYPE_MANAGE     = "支出类型管理";
// 树形菜单定义 map
std::map<TreeItemName_t, StackWidgetIndex_t> TREEITEM_MAP = {
    { TREEITEM_OVERVIEW,                0 },
    { TREEITEM_CLIENT,                  -1 },
    { TREEITEM_REVENUE_EXPEND,          1 },
    { TREEITEM_SYSTEM_SETTING,          -1 },
    { TREEITEM_POSITION_MANAGE,         2 },
    { TREEITEM_EXPEND_TYPE_MANAGE,      4 },
};

}

#endif // DEF_H
