#ifndef DIALOG_ADD_POSITION_H
#define DIALOG_ADD_POSITION_H

#include <QDialog>

namespace Ui {
class Dialog_add_position;

// 数据传输对象
struct Dialog_add_position_DTO {
    qint64 id = 0;
    QString name = "";
    QString remark = "";
    QString createTime = "";
};

}

class Dialog_add_position : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_add_position(QWidget *parent = nullptr);
    ~Dialog_add_position();

    // 设置数据
    void setData(Ui::Dialog_add_position_DTO dto);

    // 获取数据
    Ui::Dialog_add_position_DTO getData();

signals:
    void do_getData(const Ui::Dialog_add_position_DTO& dto);
    void do_setData(const Ui::Dialog_add_position_DTO& dto);
    void do_deleteData(const Ui::Dialog_add_position_DTO& dto);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_lineEdit_position_name_textChanged(const QString &arg1);

    void on_lineEdit_remark_textChanged(const QString &arg1);

    void on_pushButton_deletePosition_clicked();

private:
    Ui::Dialog_add_position *ui;

    // 数据对象
    Ui::Dialog_add_position_DTO dto;

    // 重写accept
    void accept();
};

#endif // DIALOG_ADD_POSITION_H
