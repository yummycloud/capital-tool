#ifndef UTILS_H
#define UTILS_H
#include <QString>
#include <QDateTime>

namespace Utils
{

QString getCurrentDatetimeStr()
{
    return QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
}

}

#endif // UTILS_H
