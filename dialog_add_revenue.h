#ifndef DIALOG_ADD_REVENUE_H
#define DIALOG_ADD_REVENUE_H

#include <QDialog>

namespace Ui {

class Dialog_add_revenue;

// 数据传输对象
struct Dialog_add_revenue_DTO {
    QString phone           = "";
    QString name            = "";
    bool    isVip           = false;
    QString isVipName       = "";       // 转换为名称
    qint64  position        = -1;
    QString positionName    = "";       // 转换为名称
    double  expendMoney     = 0.0;
    double  incomeMoney     = 0.0;
};

}

class Dialog_add_revenue : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_add_revenue(QWidget *parent = nullptr);
    ~Dialog_add_revenue();

    // 获取表单字段
    Ui::Dialog_add_revenue_DTO getData();

    // 设置表单字段(编辑)
    void setData(Ui::Dialog_add_revenue_DTO& dto);


private:
    Ui::Dialog_add_revenue *ui;

    // 标记当前是否为编辑模式
    bool    isEdit = false;

    // 字段
    struct Ui::Dialog_add_revenue_DTO dto;
};

#endif // DIALOG_ADD_REVENUE_H
